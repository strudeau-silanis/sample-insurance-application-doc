@startuml
actor user

user -> app: user completes insurance form

participant "esl\nwithin iFrame" as esl
app -> esl: application creates package on e-SL
note right: package defines handover URL link.
esl --> app
app --> user: application displays eSL signing\n ceremony within an iFrame.
user -> esl: user completes signing ceremony.
user -> esl: user clicks on handover URL link.

participant "AutoSubmit form\nwithin iFrame" as iframe
esl -> iframe: eSL redirects iFrame to an Autosubmit\n form within the sample insurance application domain.

participant "parent frame" as parentFrame
iframe -> parentFrame: autosubmit form invokes parent frame javascript

participant "final destination" as final
parentFrame --> final: redirect user to final destination.
@enduml
