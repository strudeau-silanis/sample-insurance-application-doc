REM Get plantuml from http://plantuml.sourceforge.net/command_line.html
java -jar plantuml.jar -tsvg "*.txt"
java -jar plantuml.jar -tpng "*.txt"
REM Get inkscape from http://inkscape.org/
inkscape "iframe-autosubmit-diag.svg" --export-pdf="iframe-autosubmit-diag.pdf"