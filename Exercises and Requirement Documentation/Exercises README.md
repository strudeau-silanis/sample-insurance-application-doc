# User IT Conference #

----------

# Prerequisites #

1. <a href="https://www.silanis.com/products/edition-and-pricing/sign-up-for-e-signlive/integrated/step1" target="_blank">Create a Sandbox Account</a>- free trial account for SDK integrated customers
2. Get your API Key: Once logged in, click on the **Account** tab from the e-SignLive Toolbar and click the **Unlock** tab

![apikey.png](images/apikey.png)

3. Install an IDE based on your programming language (C# or Java) 

4. Download exercise source code
	- <a href="https://bitbucket.org/sgupta13/sample-insurance-app-net/downloads" target="_blank">C# </a>
	- <a href="https://bitbucket.org/strudeau-silanis/sample-insurance-application/downloads">Java</a>

**Note:** For those who have not had a chance to prepare the pre-requisites, ask the instructor for a USB stick. 

----------	

## Useful Links ##

### C# ###

- <a href="http://sample-esl-application.apphb.com/" target="_blank"> Sample Insurance Application C# </a> 
- <a href="https://github.com/silanis/esl.sdk.net" target="_blank"> esl.sdk.net GitHub Respository </a>
- <a href="http://docs.e-signlive.com/10.0/sdk/esl-sdk-net-10.4.2.2.zip" target="_blank">.NET SDK</a>
- <a href="https://github.com/silanis/esl.sdk.net/tree/master/sdk/SDK.Examples/src" target="_blank">.NET Examples</a>

#### Development Tools ####
- <a href="http://www.microsoft.com/en-ca/download/details.aspx?id=40747" target="_blank"> Microsoft Visual Studio Express 2013 for Web </a> 

#### Hosting Tools #### 
- <a href="https://github.com/appharbor/appharbor-cli/downloads" target="_blank"> AppHarbor CLI </a>

-----------

### Java ###

- <a href="http://mysterious-shore-4556.herokuapp.com/" target="_blank"> Sample Insurance Applicationc Java </a>
- <a href="https://github.com/silanis/esl.sdk.java" target="_blank"> esl.sdk.javae-SignLive SDK GitHub Respository </a>
- <a href="http://docs.e-signlive.com/10.0/sdk/esl-sdk-java-10.4.2.2.zip" target="_blank"> Java SDK </a>
- <a href="https://github.com/silanis/esl.sdk.java/tree/master/sdk/src/main/java/com/silanis/esl/sdk/examples" target="_blank"> Java Examples </a>

#### Development Tools ####
- <a href="http://spring.io/tools/sts/all" target="_blank">Spring Tool Suite</a> 
- <a href="http://tomcat.apache.org/download-70.cgi" target="_blank">Apache Tomcat 7</a>

#### Hosting Tools ####
- <a href="https://toolbelt.heroku.com/" target="_blank">Heroku Toolbelt</a>

## Compiling the Application under Java ##

This Sample Insurance Application is a standard java web application that was tested to run on tomcat 7, openshift (tomcat 7), heroku (jetty 9.x). 
In order to compile and run for the right deployment environment, using MAVEN enable the profile corresponding to your environment.

- tomcat
- openshift
- heroku

E.g.:

```
$ mvn -P tomcat clean package
```

### Note ###

You don't need to explicitly specify a maven profile when deploying to openshift or heroku, the code is configured to use the right profile automatically.

----------

# Exercise 1: Dynamic Package Creation #

- <a href="https://bitbucket.org/sgupta13/exercise-1-dynamic-package-creation-net/downloads" target="_blank">Exercise 1 Git Repository - C#</a>
- <a href="https://bitbucket.org/strudeau-silanis/exercise-1-dynamic-package-creation/downloads" target="_blank">Exercise 1 Git Repository - java</a>
- 
## 1.1 Objective ##

From the sample insurance application website, participants fill in the insurance form and submit the form to the application. In the background, the application takes the information collected and submits those information to e-SignLive in order to customize the insurance contract document and invoke e-SignLive for the participants to sign their insurance contract online.

The objective of this exercise is to create an e-SignLive document package and familiarize the participants with the e-SignLive SDK methods used to achieve the following:

- Add signers to a document package.
- Create signatures for one or multiple signers.
- Add fields to a document.
- Enable field injection to insert user values into a document.
- Customize the ceremony layout.

## 1.2 Background ##
### Basic Components of a Package ###

The basic components of a document package are:

- The Sender
- The Signer (one or multiple signers allowed)
- The Documents to be signed
	- Signatures
		- Bound fields (signature date, etc.)
		- Unbound fields (checkboxes, textboxes)
	- Injected fields
- UI elements customization (ceremony layout)

## 1.3 Exercise Steps ##

### 1.3.1 Step 1: Define the Sender ###

To create an e-SignLive Package, you are required to enter the API URL and API KEY. 
By default, all packages created with a specific API KEY are deemed to be done on behalf of the user account for which that key was created and hence, that user is assigned the role of sender.

Enter their API URL and API KEY by accessing the application URL below:
 
Java and C#

Access `http://localhost:8080/<your application context>`

**Note** that the API URL and API KEY settings are not persistent and will need to be provided again after each re-start.

The `AutoSubmit` URL is used for iFrame integration which will be explained during a walk through. This URL is typically `http://<host>:<port>/<context>/AutoSubmit`. 

#### 1.3.2 Step 2: Customize the Document Package Settings ####

Open the ESLPackageCreation.cs or ESLPackageCreation.java file in your IDE. Note: You will be editing this file for the rest of Exercise 1. Look out for the comments titled "TODO" for the exercise steps.

e-SignLive SDK offers a `DocumentPackageSettingsBuilder` that allows you to customize the document package settings according to your requirements.
Please refer to the e-SignLive User Guide section called <a href="http://docs.e-signlive.com/doku.php?id=esl:e-signlive_guide_customizing-the-signing-ceremony" target="_blank">Customizing the Signing Ceremony"</a> for more information.

In the source code provided for Exercise 1, several customization options have already been set, as shown below:

C#

```csharp
// Customizing setting
                .WithSettings(DocumentPackageSettingsBuilder.NewDocumentPackageSettings()
                                .WithDecline()
                                .WithOptOut()
                                .WithDocumentToolbarDownloadButton()
                                .WithHandOverLinkHref(AUTO_SUBMIT_URL)
                                 //TODO: Customize the HandOverLinkText and HandOverLinkTooltip
 								.WithDialogOnComplete()
                                 //TODO: Enable in-person signing
```

Java

```java
// Customizing setting
				.withSettings(newDocumentPackageSettings()
								.withDecline()
								.withOptOut()
								.withDocumentToolbarDownloadButton()
								.withHandOverLinkHref(AutoSubmitURL)
								//TODO: Customize the HandOverLinkText and HandOverLinkTooltip
								.withDialogOnComplete()
								//TODO: Enable in-person signing
```

You are required to customize the <a href="http://docs.e-signlive.com/doku.php?id=esl:e-signlive_guide_customizing-the-signing-ceremony#handover_url" target="_blank">Handover URL</a> text and tooltip and enable in-person signing.

Complete this step using the appropriate <a href="http://docs.e-signlive.com/doku.php?id=esl:e-signlive_guide_customizing-the-signing-ceremony#document_package_level_settings" target="_blank">DocumentPackageSettingsBuilder Method</a>.

References

- C# <a href="https://github.com/silanis/esl.sdk.net/blob/dev/sdk/Silanis.ESL.SDK/src/Builder/DocumentPackageSettingsBuilder.cs" target="_blank">DocumentPackageSettingsBuilder.cs</a>
- Java <a href="https://github.com/silanis/esl.sdk.java/blob/master/sdk/src/main/java/com/silanis/esl/sdk/builder/DocumentPackageSettingsBuilder.java" target="_blank">DocumentPackageSettingsBuilder.java</a>

#### 1.3.3 Step 3: Signer ####
A document package can be issued to one or multiple signers at the same time. 

A signer is already added to the document package in the source code provided as shown below. You are required to add an additional signer. Note that the signer first name, last name and email ID are required.

```csharp
// Define the insured first and last name
                .WithSigner(SignerBuilder.NewSignerWithEmail(emailAddress)
                                         .WithCustomId(CUSTOM_ID)
                                         .WithFirstName(firstName)
                                         .WithLastName(lastName))
                //TODO: Add an additional signer
```

```java
// Define the insured first and last name
				.withSigner(newSignerWithEmail(emailId)
								.withCustomId("signer1")
								.withFirstName(firstName)
								.withLastName(lastName))
				//TODO: Add an additional signer
```
#### 1.3.4 Step 4: Text Field ####
Textbox and checkbox are the two types of unbound <a href="http://docs.e-signlive.com/doku.php?id=esl:e-signlive_guide_fields" target="_blank">fields</a> supported by the e-SL SDK. 

In the source code provided, checkbox and signature date fields have been added to the document. You are required to add a textfield in a similar fashion. 

```csharp
//TODO: Add a textField with name "ExtraInfo"

        .WithField(FieldBuilder.SignatureDate()
        .WithPositionExtracted()
        .WithName("Date"))
        .WithField(FieldBuilder.CheckBox()
                               .WithPositionExtracted()
							   .WithName("checkbox")))
```

```java
//TODO: Add a textField with name "ExtraInfo"

	.withField(signatureDate()
	.withPositionExtracted()
	.withName("Date"))
	.withField(checkBox()
           		.withPositionExtracted()
            	.withName("checkbox"))
```
 
#### 1.3.5 Step 5: Signature ####                                                                  

You are required to add a signature field using the X Y coordinates. Follow the <a href="http://docs.e-signlive.com/doku.php?id=esl:e-signlive_guide_signatures#examplesignatures" target="_blank">example</a> to complete this step. Note: use the email ID of the new signer added in Step 3.

#### 1.3.6 Step 6: Injected Field #### 
                                                                  
In the source code provided, many of the form fields filled in by the Insured are merged onto the document. You are required to merge the injected text field titled "VehicleColor." 

## 1.4 Solution ##
- <a href="https://bitbucket.org/sgupta13/exercise-1-dynamic-package-creation-net/downloads" target="_blank">C# Repository</a>
- <a href="https://bitbucket.org/strudeau-silanis/exercise-1-dynamic-package-creation/downloads" target="_blank"> Java Repository</a>

----------

# IFrame Integration #

## 2.1 Objective ##
The objective is to integrate e-SignLive IFrame into the sample insurance application and achieve the following: 

- Once the user completes the insurance form, route him to the e-SignLive IFrame signing ceremony.
- After signing redirect the user to a destination page based on the resulting e-SignLive package status.

##2.2 Guidelines##

### 2.2.1 Package Settings: Handover URL###

Once a user completes the signing ceremony, the **parent frame** must be redirected to an external destination. Navigation to an external destination is accomplished by setting a Handover URL in the <a target="_blank" href="http://docs.e-signlive.com/doku.php?id=esl:e-signlive_guide_customizing-the-signing-ceremony#document_package_level_settings"> package settings </a> before creating a package.

It is possible to define a HandOver URL using the sample code below:

java

```java
.withSettings( newDocumentPackageSettings()
.withHandOverLinkHref(HAND_OVER_LINK_HREF)
.withHandOverLinkText(HAND_OVER_LINK_TEXT)
.withHandOverLinkTooltip(HAND_OVER_LINK_TOOLTIP)
```

C#

```csharp
.WithSettings(DocumentPackageSettingsBuilder.NewDocumentPackageSettings()
.WithHandOverLinkHref(HAND_OVER_LINK_HREF)
.WithHandOverLinkText(HAND_OVER_LINK_TEXT)
.WithHandOverLinkTooltip(HAND_OVER_LINK_TOOLTIP)

```

The signer is redirected to a URL that contains the original URL plus additional query *parameters*:

- *package*- Package ID
- *signer*- Signer ID
- *email*- Signer email
- *status*- Package status     

### 2.2.2 Session Creation ###
In order for a user to sign, a session must be created for them after creating and sending the package. Creating a session generates a **SessionToken** object, using which an e-SignLive signing ceremony IFrame can be generated.

It is possible to create a session and generate a session token using the sample code below:

Java

```java
PackageId packageId = eslClient.createPackage( documentPackage );
eslClient.sendPackage( packageId );
String pkgId = packageId.toString();
SessionToken sessionToken = eslClient.getSessionService().createSessionToken( pkgId, signerId );
```

C#

```csharp
PackageId packageId = client.CreatePackage(package);
client.SendPackage(packageId); 
String pkgId = packageId.ToString();
SessionToken sessionToken = client.SessionService.CreateSessionToken(pkgId, SIGNER_ID);
```

###2.2.3 Accessing The Signing Ceremony###

The user is able to access the signing ceremony once a session and a session token object have been created. With the session token, the user can sign via an IFrame by following the sample HTML code below:

```html
<div align="center">
    <iframe src="https://sandbox.e-signlive.com/access?sessionToken=${token}" width="1050" height="1250" scrolling="yes"></iframe>
</div>
```

### 2.2.4 Integration ###

Once signing is complete, the user is redirected to the handover URL specified in 2.2.1. In order to redirect the entire page and not just the IFrame, we need to work around the browser same-origin policy. The page within the handover URL must contain an auto submit form which will subsequently load the entire page to a new destination. The destination is determined by using the e-SignLive package status.

In summary, the handover URL page 

- Is located within the same domain as your parent frame.
- Acts as a transition (redirection page) from e-SignLive signing ceremony to the external destination.
- Retrieves the package status by parsing the `GET` request for the *status* parameter.
- Contains Javascript invoked on page load, that calls a function on the parent page to redirect the user to a new destination based on the package status.
    - Packages with status `PACKAGE_COMPLETE` or `SIGNER_COMPLETE` return a page confirming completion of the signature process.

    - Packages with status `PACKAGE_DEACTIVATE`, `PACKAGE_DELETE` or `PACKAGE_OPTOUT` return a page indicating interruption of the signing ceremony.

Java

Refer to the following in the java project provided:

- `ViewController.java` which gets the package status by parsing the GET request for the *status* parameter and based on it directs the user to either `AutoSubmitOnComplete.jsp` or `AutoSubmitOnOther.jsp`. 

- `AutoSubmitOnComplete.jsp` which directs the signer to a page confirming the completion of the signing successfully. You can also check `Congratulations.jsp` for the content of the signing confirmation page. 

- `AutoSubmitOnOther.jsp` which directs the signer to a page informing the signer of the interruption of the signing process. You can also check `InterruptedSigning.jsp` for the content of the signing interruption page. 

C#

Refer to the following in the C# project provided:

- `AutoSubmitController.cs` which gets the package status by parsing the GET request for the *status* parameter and based on it directs the user to either `AutoSubmitOnComplete.cs` or `AutoSubmitOnOther.cs`.

- `AutoSubmitOnOther.cshtml` which directs the signer to a page confirming the completion of the signing successfully. You can also check `Congratulations.cshtml` for the content of the signing confirmation page.

- `AutoSubmitOnComplete.cshtml` which directs the signer to a page informing the signer of the interruption of the signing process. You can also check `InterruptedSigning.cshtml` for the content of the signing interruption page.

----------

# Exercise 3: Event Notifications #

![EventNotificationsFlowDiagram.svg](images/EventNotificationsFlowDiagram.svg)

## 3.1 Objective ##

The objective of this exercise is to demonstrate the use of e-SignLive SDK to successfully register for, receive and parse event notifications in order to retrieve package statuses. In order to demonstrate, you will be deploying the application to a cloud based hosting service in order to receive eSL notifications.

##3.2 Background##
The e-SignLive system allows integrators of back end systems to be notified of events pertaining to a package. Upon a specific <a href="http://docs.e-signlive.com/doku.php?id=esl:e-signlive_guide_event-notification#events" target="_blank">event</a>, the system will issue a message automatically to a destination of the integrator's choice.

##3.3 Exercise Steps##

### 3.3.1 Deploy your application on the Cloud###
Deploy the source code on Heroku (Java) or AppHarbor (C#):

#### C#: How To Deploy on APPHARBOR ####

1. Create an <a href="https://appharbor.com/user/new" target="_blank">AppHarbor account</a>
2. Download an install the <a href="http://blog.appharbor.com/2012/4/25/introducing-the-appharbor-command-line-utility" target="_blank">AppHarbor CLI</a> if you have not done so already
3. Create a root directory for this project on your computer.
4. Checkout the project using git:

        git clone https://bitbucket.org/sgupta13/exercise-3-event-notifications-net.git

5. Navigate to this project root directory (referred to as $) and login to AppHarbor

        $ appharbor user login
        Username: example
        Password:
        Successfully logged in as example

5. Create a new application on Appharbor

        $ appharbor create insurance
        Created application "insurance" | URL: https://insurance.apphb.com

6. Deploy your code onto the application you just created

        $ appharbor app deploy
        Getting upload credentials..

        Uploading package <100% of 25.1 MB>. 0 seconds left
        The package will be deployed to application "insurance".

        Enter a deployment message: initial commit
        Notifying AppHarbor.
        Deploying...Open application overview with 'appharbor open'.

7. Open your application in a web browser with the following command:

        $ appharbor open

8. Congratulations, your application should now be running on AppHarbor.

#### Java: How To Deploy on HEROKU ####

1. Create a <a href="www.heroku.com" target="_blank">heroku</a> account
2. Download and install the <a href="https://toolbelt.heroku.com/" target="_blank">heroku tool belt</a> if you have not done so already. This will install both the Heroku CLI and GIT command line client.
3. Create a root directory for this project on your computer.
4. Checkout the project using git:

        git clone https://bitbucket.org/strudeau-silanis/exercise-3-event-notifications.git

5. From this project root directory referred to as `$` in the example below.
6. Login and create an SSH key:

        $ heroku login
        Enter your Heroku credentials.
        Email: adam@example.com
        Password:
        Could not find an existing public key.
        Would you like to generate one? [Yn]
        Generating new SSH public key.
        Uploading ssh public key /Users/adam/.ssh/id_rsa.pub
	
7. Create an app on Heroku:

        $ heroku create

8. Define a MAVEN settings file to use the heroku maven profile

        $ heroku config:set MAVEN_SETTINGS_PATH=.heroku/settings.xml
	
9. Push your application to heroku. You should now see your code being uploaded to heroku and the application being built.

        $ git push heroku master

10. Open the application using the system default browser.

        $ heroku open

11. Repeat step 9-10 above if you make code changes.
12. Congratulation, your application should now be running in heroku!

### 3.3.2 Register ###
The first step is to register a URL to be notified in case of an event. With a single request, an account can have one or more events linked to a URL.
 
Register for event notifications from your sandbox account. Log in to your sandbox account and access the **Acount** page from the e-SignLive Toolbar as seen below:

![account.png](images/account.png)
  
Enter the Callback URL you would like the notifications to be sent to. For purposes of this exercise, enter `https://<host>:<port>/<context>/ESLNotifications`.

Under **Select events**, select the box belonging to the event you wish to register.

![notifications.png](images/notifications.png)

###3.3.3 Receive###
Once you have registered to be notified of one or more events at the registered URL, you wait while listening for the notification. 

When an event is triggered, the system issues an HTTP POST to the registered URL. Contained within this message is a payload in JSON format, which describes the particular event that triggered the notification.

An example payload is shown below: 

```json
{
"name": "PACKAGE_ACTIVATE",
"sessionUser": "XXXXXXXXXXXX",
"packageId": "XXXXXXXXXXXXXXXXXXXXXXXXXXXX"
}
```
As seen in the payload sample shown , note that the *name* parameter denotes the status of the package, uniquely identified by the *packageId* parameter.
Once you receive a notification, you are able to parse the JSON response to retrieve the package ID and status. 

Go to **Controllers/ESLNotificationsController.cs** or **Controllers/ESLNotificationsController.java** and insert a trace statement to confirm that the notifications are being received.

### 3.3.4 Retrieve ###
Go to the **Notifications Dashboard** to view all the packages along with their statuses and download documents, evidence and fields values for the completed packages. 

![dashboard.png](images/dashboard.png)

----------

##3.4 Solution ##
- <a href="https://bitbucket.org/sgupta13/exercise-3-event-notifications-net/downloads" target="_blank">C# Repository</a> 
- <a href="https://bitbucket.org/strudeau-silanis/exercise-3-event-notifications/downloads" target="_blank">Java Repository</a>